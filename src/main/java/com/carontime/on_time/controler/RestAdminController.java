package com.carontime.on_time.controler;

import com.carontime.on_time.dto.CarDataDto;
import com.carontime.on_time.model.car.Car;
import com.carontime.on_time.model.car.CarStatus;
import com.carontime.on_time.service.carservice.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.awt.geom.Point2D;
import java.io.IOException;
import java.util.Map;

@RestController
@RequestMapping("/rest/admin")
public class RestAdminController {

    private CarService carService;

    @Autowired
    public RestAdminController(CarService carService) {
        this.carService = carService;
    }

    @GetMapping("/getcar/{carid}")
    public String getCarData(@PathVariable("carid") int carid) {
        Car car = carService.getCar(carid).orElse(new Car(0, null, new Point2D.Double()));
        return CarDataDto.getJSJson(new CarDataDto(car));
    }

    @PostMapping("/setcar")
    public void setCarData(HttpServletRequest request) throws IOException {
        Map<String, String[]> params = request.getParameterMap();
        Car parseCarData = requestDataToCar(request);
        Car car = carService.getCar(Integer.parseInt(params.get("id")[0])).orElseThrow(RuntimeException::new);
        carService.updateCar(car, parseCarData);
    }

    @PostMapping("/delcar")
    public void delCarData(HttpServletRequest request) throws IOException {
        Map<String, String[]> params = request.getParameterMap();
        Car car = carService.getCar(Integer.parseInt(params.get("id")[0])).orElseThrow(RuntimeException::new);
        carService.deleteCar(car);
    }

    private Car requestDataToCar(HttpServletRequest request) {
        Car car = null;
        Map<String, String[]> params = request.getParameterMap();

        try {
            car = new Car();

            car.setStatus(CarStatus.valueOf(params.get("status")[0]));
            car.setLocalization(
                    new Point2D.Double(
                            Double.parseDouble(params.get("car_x")[0]),
                            Double.parseDouble(params.get("car_y")[0])
                    )
            );
        } catch(NumberFormatException | NullPointerException ignored) {
            throw new RuntimeException();
        }

        return car;
    }

}
