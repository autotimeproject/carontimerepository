package com.carontime.on_time.model.result;

import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.stream.Collectors;

public class ErrorParser {

    public static final int MODEL_ATTR_NO = 0;
    public static final int MODEL_ATTR_NORMAL = 1;
    public static final int MODEL_ATTR_FLASH = 2;

    private BindingResult bindingResult;
    private Model model;
    private RedirectAttributes flashModel;
    private String fieldName = "errors";

    private ErrorParser(BindingResult bindingResult, Model model, String fieldName) {
        this.bindingResult = bindingResult;
        this.model = model;
        this.fieldName = fieldName;
        if(model != null) model.addAttribute(fieldName, getErrors());
    }

    private ErrorParser(BindingResult bindingResult, RedirectAttributes model, String fieldName) {
        this.bindingResult = bindingResult;
        this.flashModel = model;
        this.fieldName = fieldName;
        model.addFlashAttribute(fieldName, getErrors());
    }

    public boolean hasErrors() {
        return bindingResult.hasErrors();
    }

    public List<String> getErrors() {
        return bindingResult.getAllErrors().stream().map(DefaultMessageSourceResolvable::getDefaultMessage).collect(Collectors.toList());
    }

    public String getFieldName() {
        return fieldName;
    }

    public static ErrorParser ofSimpleModel(BindingResult bindingResult, Model model, String fieldName) {
        return new ErrorParser(bindingResult, model, fieldName);
    }

    public static ErrorParser ofFlashModel(BindingResult bindingResult, RedirectAttributes model, String fieldName) {
        return new ErrorParser(bindingResult, model, fieldName);
    }




}
