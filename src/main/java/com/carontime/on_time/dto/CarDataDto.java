package com.carontime.on_time.dto;

import com.carontime.on_time.model.car.Car;
import com.carontime.on_time.model.car.CarStatus;

import java.util.Locale;

public class CarDataDto {
    private int carid;
    private CarStatus status;
    private double posx;
    private double posy;

    public CarDataDto(Car car) {
        this.carid = car.getId();
        this.status = car.getStatus();
        this.posx = car.getX();
        this.posy = car.getY();
    }

    public int getCarid() {
        return carid;
    }

    public void setCarid(int carid) {
        this.carid = carid;
    }

    public CarStatus getStatus() {
        return status;
    }

    public void setStatus(CarStatus status) {
        this.status = status;
    }

    public double getPosx() {
        return posx;
    }

    public void setPosx(double posx) {
        this.posx = posx;
    }

    public double getPosy() {
        return posy;
    }

    public void setPosy(double posy) {
        this.posy = posy;
    }

    public static String getJSJson(CarDataDto carDataDto) {
        return String.format(Locale.US,
                "{" +
                            "\"carid\":\"%d\"," +
                            "\"status\":\"%s\"," +
                            "\"x\":\"%.6f\"," +
                            "\"y\":\"%.6f\"" +
                        "}",
                carDataDto.getCarid(),
                carDataDto.getStatus(),
                carDataDto.getPosx(),
                carDataDto.getPosy()
        );
    }
}
