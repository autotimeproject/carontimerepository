package com.carontime.on_time.service.carservice;

import com.carontime.on_time.forms.CarForm;
import com.carontime.on_time.model.car.Car;

import java.util.List;
import java.util.Optional;

public interface CarService {
    Car addCar(Car car);
    void deleteCar(Car car);
    Optional<Car> getCar(Integer id);
    List<Car> getAll();
    boolean isExists(Integer id);
    void updateCar(Integer car, CarForm carForm);
    void updateCar(Car toUpdate, Car dataFrom);
}
