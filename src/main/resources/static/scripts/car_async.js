var pageUrl = "http://localhost:8083/rest/admin";

var lastQueryData = {
    id: 0,
    status: null,
    car_x: 0.0,
    car_y: 0.0
};

function loadDataForCar(carid) {
    $.ajax(pageUrl + "/getcar/" + carid)
           .done(function(data) {
                var json = jQuery.parseJSON(data);
                lastQueryData.id = json.carid;
                lastQueryData.status = json.status;
                lastQueryData.car_x = json.x;
                lastQueryData.car_y = json.y;

                updateEditForm();
                updateDeleteForm();
           });
}

function updateEditForm() {
    $('#ed_form_car_id').val(lastQueryData.id);
    $('#ed_form_status').val(lastQueryData.status);
    $('#ed_form_lan').val(lastQueryData.car_x);
    $('#ed_form_lon').val(lastQueryData.car_y);
}

function updateDeleteForm() {
    $('#delInfo').text("Czy na pewno chcesz usunać pojazd o ID: " + lastQueryData.id + " ?");
}

function saveDataForCar() {
    lastQueryData.status = $('#ed_form_status').val();
    lastQueryData.car_x = $('#ed_form_lan').val();
    lastQueryData.car_y = $('#ed_form_lon').val();
    sendQuery("setcar", "editResult");
}

function deleteCar() {
    sendQuery("delcar", "delResult");
}

function sendQuery(mapping, resultobj) {
    $.ajax({
      type: "POST",
      url: pageUrl + "/" + mapping,
      data: lastQueryData,
      success: function() {
        hideModalWindow();
        location.reload();
      },
      error: function() {
        $("#" + resultobj).text("Wystąpił błąd przy przetwarzaniu formularza...");
      }
    });
}