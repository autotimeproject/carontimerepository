/*
    Skrypt wymaga dodania do 'wyzwalacza' okienka modalnego atrybutu:
        onclick="showAsModalWindow('idElementuZawierajacegoDaneDoPokazania')"
    oraz do 'zamykacza' atrybutu:
        onclick="hideModalWindow()"
    Ponadto do stylów powinien być dodany plik 'modal-window.css'.

*/
var modalWindow = null;

function showAsModalWindow(modalBlockElementId) {
    hideModalWindow();

    modalWindow = $("#" + modalBlockElementId);
    modalWindow.show();
    modalWindow.click(function(event) {
        if(event.target.id == modalWindow[0].id) hideModalWindow();
    })
}

function hideModalWindow() {
    if(modalWindow != null) {
        modalWindow.hide();
    }
}